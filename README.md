

# Newtons Cradle in Python (PyNewt)

## Background Information:


 - Circular Motion
 - Momentum
 - Energy

## Decisions:
Classes have been created for the Ball and Cradle. With the ball primarily acting as a container class and the Cradle handling the movement, collisions and rendering (or maybe renderer is a separate class?)
Each ball operates in its own coordinate system, with the origin (0,0) occuring at the pivot of each ball
Angles are handled in degrees for accessibility for less mathematically literate users
Gravity is approximated to be 9.8ms^-2


## Versions:
 * ### Version 0: Pendulum
Initial implementation still utilises the cradle and ball class structure but uses a single ball to establish the basic physic engine and how that is effectively rendered and displayed to the user. Most variables are implemented as constants this includes: the length of the string, the starting angle of the ball is set to 90. The ball has no radius or mass (as this is irrelevant without collisions) and exists as a point. There is perfect efficiency in energy transfer (from gravitational potential to kinetic and back).

	- Master
 
![pygameimp](https://i.imgur.com/kTjy3AN.png "title")

_Figure 1: Pygame implementation of the first stage_

 * ### Version 1: Basic Cradle
The user may select the number of balls and collisions are implemented, transferring their velocity (rather than their momentum) upon impact. At rest each ball is perfectly touching its neighbours, and in the same line. The user may only start the simulation with one ball elevated to simplify the collisions. Collisions are perfectly elastic and therefore only one ball has momentum at once. This means only one ball�s movement needs to be simulated at once.

	- Master
	- Collisions

 * ### Version 2: User Defined Specifications
Multiple balls can be elevated at the start of the simulation. The balls now have individual radii, which can be selected by the user. Collisions now transfer energy rather than velocity and must allow for the radius of the balls. The user can also adjust the length of the strings attached to the balls, although this is constants for all the balls.

	- Collisions
	- Physics

 * ### Version 3: Finer Details
The user may now optionally define string mass which influences arc movement. Energy transfer is no longer perfectly elastic but dependent on some mathematical function (potentially a ratio, but could be more complex). The user must now also specify the number of frames per second and the duration of the simulation.

	- Collisions
	- Physics
	- 

 * ### Version 4: ??? :P
	- Changing gravitational constants
	- Playing with spacing between balls
	- Visual representations of energy movement through colour (including losses to heat and sound)
	- Adjustment of efficiency of energy transfer coefficient/type/nature
	- Air resistance? (adjusting material/shape of ball?) *this is legit so small its not even funny we may aswell add friction*